defmodule BrainfuckTest do
  use ExUnit.Case
  import ExUnit.CaptureIO
  doctest Brainfuck

  test "Hello World" do
    code = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++."
    data = %Brainfuck{
      code: String.graphemes(code)
    }

    assert capture_io(fn ->
      Brainfuck.process(data)
    end) == "Hello World!\n\nFinished\n"
  end
end
