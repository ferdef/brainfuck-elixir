defmodule Brainfuck do
  defstruct(
    mem_space: List.duplicate(0, 30_000),
    mem_pointer: 0,
    code: [],
    code_pointer: 0,
    loop_pointer: []
  )

  def main(args \\ []) do
    data = %Brainfuck{
      code: String.graphemes(List.first(args))
    }
    process(data)
  end

  def process(data = %Brainfuck{code_pointer: code_pointer, code: code}) when length(code) > code_pointer do
    command = Enum.at(code, code_pointer)

    operate(command, data)
    |> move_pointer(command)
    |> process
  end

  def process(_) do
    IO.puts("\nFinished")
  end

  defp operate("+", data) do
    %Brainfuck{
      data
      | mem_space:
          data.mem_space
          |> List.update_at(data.mem_pointer, &(&1 + 1))
    }
  end

  defp operate("-", data) do
    %Brainfuck{
      data
      | mem_space:
          data.mem_space
          |> List.update_at(data.mem_pointer, &(&1 - 1))
    }
  end

  defp operate(">", data), do: %Brainfuck{data | mem_pointer: data.mem_pointer + 1}

  defp operate("<", data), do: %Brainfuck{data | mem_pointer: data.mem_pointer - 1}

  defp operate(".", data) do
    IO.write(<<Enum.at(data.mem_space, data.mem_pointer)>>)
    data
  end

  defp operate("[", data), do: data

  defp operate("]", data), do: data

  defp operate(command, data) do
    IO.puts("Unknown #{command}")
    data
  end

  defp move_pointer(data, "[") do
    loop_pointer = List.insert_at(data.loop_pointer, 0, data.code_pointer)
    %Brainfuck{data | loop_pointer: loop_pointer, code_pointer: data.code_pointer + 1}
  end

  defp move_pointer(data, "]") do
    case Enum.at(data.mem_space, data.mem_pointer) do
      0 ->
        {_, loop_pointer} = List.pop_at(data.loop_pointer, 0)
        %Brainfuck{data | code_pointer: data.code_pointer + 1, loop_pointer: loop_pointer}
      _ ->
        %Brainfuck{data | code_pointer: List.first(data.loop_pointer) + 1}
    end
  end

  defp move_pointer(data, _), do: %Brainfuck{data | code_pointer: data.code_pointer + 1}
end
