# Brainfuck

Just a quick and dirty implementation of the Brainfuck language in Elixir

The idea came from a recent Live coding done by Salvador de la Puente (salvadelapuente.com) during the WeCode Fest 2019 (https://wecodefest.com)

There's one thing still missing: `,` operand